Algoritmo XI_Seleccion_Multiple
	Escribir 'Introduzca el primer numero'
	Leer num1
	Escribir 'Introduzca el segundo numero'
	Leer num2
	Escribir 'Introduzca el tercer numero'
	Leer num3
	mayor <- 0
	mediano <- 0
	menor <- 0
	Si num1>num2 Entonces
		Si num1>num3 Entonces
			mayor <- num1
			Si num2>num3 Entonces
				mediano <- num2
				menor <- num3
			SiNo
				mediano <- num3
				menor <- num2
			FinSi
		SiNo
			mayor <- num3
			mediano <- num1
			menor <- num2
		FinSi
	SiNo
		Si num2>num3 Entonces
			mayor <- num2
			Si num1>num3 Entonces
				mediano <- num1
				menor <- num3
			SiNo
				mediano <- num3
				menor <- num1
			FinSi
		SiNo
			mayor <- num3
			mediano <- num2
			menor <- num1
		FinSi
	FinSi
	Escribir 'Orden ascendente: ',menor," ",mediano," ",mayor
	Escribir 'Orden descendente: ',mayor," ",mediano," ",menor
FinAlgoritmo

