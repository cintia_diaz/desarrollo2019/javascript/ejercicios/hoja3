Algoritmo IX_Operadores
	Escribir "Introduzca un numero"
	Leer num1
	Escribir "Introduzca otro numero"
	Leer num2
	suma <- 0
	resta <- 0
	producto <- 0
	division <- 0
	restoDivision <- 0
	divisionEntera <- 0
	potencia <- 0
	
	suma <- num1+num2
	resta <- num1-num2
	producto <- num1*num2
	division <- num1/num2
	restoDivision <- num1%num2
	divisionEntera <- (num1-restoDivision)/num2
	potencia <- num1^num2
	
	Escribir "La suma es ", suma
	Escribir "La resta es ", resta
	Escribir "El producto es ", producto
	Escribir "La division es ", division
	Escribir "El resto de la division entera es ", restoDivision
	Escribir "La division entera es ", divisionEntera
	Escribir num1, " elevado a ", num2 " es ", potencia
	
	
FinAlgoritmo
