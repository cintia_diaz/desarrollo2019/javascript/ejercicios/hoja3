Algoritmo VI_Seleccion_simple
	Escribir "Escribe un numero"
	Leer num1
	Escribir "Escribe otro numero"
	Leer num2
	Escribir  "Escribe el ultimo numero"
	Leer num3
	menor <-0
	Si num1<num2 Entonces
		Si num1<num3 Entonces
			menor<-num1
		SiNo
			menor<-num3
		FinSi
	SiNo
		Si num2<num3 Entonces
			menor<-num2
		SiNo
			menor<-num3			
		FinSi
	FinSi
	Escribir "El menor es ", menor
	
FinAlgoritmo
