Algoritmo X_Seleccion_Multiple
	Escribir "Introduzca una nota"
	Leer nota
	notaEscrita <- "Nota incorrecta"
	
	Si nota>=0 Y nota<5 entonces
		notaEscrita <- "Suspenso"
	SiNo
		Si nota>=5 y nota<6 entonces
			notaEscrita <- "Suficiente"
		SiNo
			Si nota>=6 y nota<7 entonces
				notaEscrita <- "Bien"
			SiNo
				Si nota>=7 y nota<9 entonces
					notaEscrita <- "Notable"
				Sino
					Si nota>=9 y nota<=10 Entonces
						notaEscrita <- "Sobresaliente"
					FinSi					
				FinSi
			FinSi
		FinSi
	FinSi
	
	Escribir notaEscrita
	
FinAlgoritmo
